/* Special thanks to /u/StuckOnCoboldLevel for this code which I slighty changed to make it easier to read. Find their original post at https://old.reddit.com/r/learnjavascript/comments/cdh5k9/attention_required/ettw952/ 
Since it's public on codepen(https://codepen.io/anon/pen/EBBWXY) it means it's under MIT license. */
var boody = document.querySelector('body');
var buttonss = document.querySelectorAll('button');

buttonss[0].addEventListener('click', toDark); 
buttonss[1].addEventListener('click', toLight);

/* To remember Rebecca Meyer (see https://meyerweb.com/eric/thoughts/2014/06/19/rebeccapurple/ & https://www.economist.com/babbage/2014/06/23/the-colour-purple for details) */
buttonss[2].addEventListener('click', toPurple); 

function toDark () 
{ 
    boody.className = 'dark';
    /* Set cookie named "mode" to value of "dark" */
    document.cookie = "mode=dark";
}
function toLight () 
{ 
    boody.className = 'light'; 
    /* Set cookie named "mode" to value of "light" */
    document.cookie = "mode=light";
}
/* To remember Rebecca Meyer (see https://meyerweb.com/eric/thoughts/2014/06/19/rebeccapurple/ & https://www.economist.com/babbage/2014/06/23/the-colour-purple for details) */
function toPurple () 
{ 
    boody.className = 'purple';
    /* Set cookie named "mode" to value of "purple" */
    document.cookie = "mode=purple";
}

/* Set a default cookie to light (since background is white) */ 
function setDefaultMode()
{
    document.cookie = "mode=light";
}

/* Checking to see what mode is currently set and use it across all pages. Thanks to  https://stackoverflow.com/a/58439764 
Also sets the current mode to light as default */
document.addEventListener("DOMContentLoaded", function setMode()
{
    if (document.cookie == "mode=dark")
    {
        toDark();
    }
    else if(document.cookie == "mode=purple")
    {
        toPurple();
    }
    else
    {
        toLight();
    }

});

/* Delete cookie when needed. Thanks to https://stackoverflow.com/a/58439764 */
function deleteCookie()
{
    document.cooke = mode+ '=; Max-Age=-999999;';
} 