# eventListing

Instead of relying on a site like Meetup (or Eventbrite or something similar) to show what events you have upcoming (and to RSVP), why not use your own site. Use this template and get it done. 

## Screenshots

Light mode:
![Light mode of website](https://s.put.re/CXfap14m.png)

Dark mode:
![Dark mode of website](https://s.put.re/A5EVEdNf.png)

Purple mode:
![About page on purple mode](https://s.put.re/1s579UYw.png)

## Download

You can either [clone the repo](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository#_git_cloning) to get a download or [download the entire repo from Bitbucket](https://bitbucket.org/Pronfu/eventlisting/downloads/Event_List_Site.zip).

## Live Demo

[https://projects.gregoryhammond.ca/eventlisting/](https://projects.gregoryhammond.ca/eventlisting/)

## License

[Unlicense](https://unlicense.org/)